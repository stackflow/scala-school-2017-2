package wtf.scala.lectures.e13

case class Judge(name: String, specialization: Option[String])

case class Document(ownerId: Long, content: String)
